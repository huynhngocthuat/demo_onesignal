package com.assetjapan.demo_onesignal.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Getter
@Setter
@ConfigurationProperties(prefix = "onesignal")
public class OneSignalConfigProperties {

  private String appId;

  private String appKey;
}
