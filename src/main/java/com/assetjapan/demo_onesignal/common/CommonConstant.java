package com.assetjapan.demo_onesignal.common;

public final class CommonConstant {
    public static final String SUCCESS = "success";
    public static final String FAILURE = "failure";
}
