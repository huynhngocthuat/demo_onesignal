package com.assetjapan.demo_onesignal.service;

import com.assetjapan.demo_onesignal.response.ResponseDataAPI;

import java.util.UUID;

public interface PushNotificationOneSignalService {

  void pushByExternalUserId(String content, ResponseDataAPI data, UUID recieverId);

  void pushByFilter(ResponseDataAPI data, UUID receiverId, Object value);

  void pushVoIP(String content, ResponseDataAPI data, UUID receiverId);
}
