package com.assetjapan.demo_onesignal.service.impl;

import com.assetjapan.demo_onesignal.config.OneSignalConfigProperties;
import com.assetjapan.demo_onesignal.response.ContentResponse;
import com.assetjapan.demo_onesignal.response.FilterResponse;
import com.assetjapan.demo_onesignal.response.NotificationOneSignalResponse;
import com.assetjapan.demo_onesignal.response.ResponseDataAPI;
import com.assetjapan.demo_onesignal.service.PushNotificationOneSignalService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Service;

import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.*;

@Slf4j
@Service
@RequiredArgsConstructor
@EnableConfigurationProperties(OneSignalConfigProperties.class)
public class PushNotificationOneSignalServiceImpl implements PushNotificationOneSignalService {
    private final OneSignalConfigProperties properties;

    @Override
    public void pushByExternalUserId(String content, ResponseDataAPI data, UUID receiverId) {
        this.push(this.toResponse(content, data, receiverId));
    }

    @Override
    public void pushByFilter(ResponseDataAPI data, UUID receiverId, Object value) {
        NotificationOneSignalResponse object = new NotificationOneSignalResponse();

        List<FilterResponse> listFilterResponses = new ArrayList<>();
        listFilterResponses.add(FilterResponse.filterLocation(value));

        List<UUID> listReceiver = new ArrayList<>();
        listReceiver.add(receiverId);

        object.setIncludeExternalUserIds(listReceiver);
        object.setData(data);
        object.setFilters(listFilterResponses);
        object.setAndroidBackgroundData(true);
        object.setContentAvailable(true);
        this.push(object);
    }

    @Override
    public void pushVoIP(String content, ResponseDataAPI data, UUID receiverId) {
        NotificationOneSignalResponse object = this.toResponse(content, data, receiverId);
//        object.setApnsPushTypeOverride("voip");
        object.setAndroidBackgroundData(true);
        this.push(object);
    }

    private void push(NotificationOneSignalResponse notificationOneSignalResponse) {
        try {
            String jsonResponse;

            URL url = new URL("https://onesignal.com/api/v1/notifications");
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setUseCaches(false);
            con.setDoOutput(true);
            con.setDoInput(true);

            con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            con.setRequestProperty("Authorization", "Basic " + properties.getAppKey());
            con.setRequestMethod("POST");

            notificationOneSignalResponse.setAppId(properties.getAppId());

            byte[] sendBytes =
                    Objects.requireNonNull(this.convertToJSONString(notificationOneSignalResponse))
                            .getBytes(StandardCharsets.UTF_8);
            log.debug(this.convertToJSONString(notificationOneSignalResponse));
            con.setFixedLengthStreamingMode(sendBytes.length);
            OutputStream outputStream = con.getOutputStream();
            outputStream.write(sendBytes);

            int httpResponse = con.getResponseCode();

            if (httpResponse >= HttpURLConnection.HTTP_OK
                    && httpResponse < HttpURLConnection.HTTP_BAD_REQUEST) {
                Scanner scanner = new Scanner(con.getInputStream(), StandardCharsets.UTF_8);
                jsonResponse = scanner.useDelimiter("\\A").hasNext() ? scanner.next() : "";
                scanner.close();
            } else {
                Scanner scanner = new Scanner(con.getErrorStream(), StandardCharsets.UTF_8);
                jsonResponse = scanner.useDelimiter("\\A").hasNext() ? scanner.next() : "";
                scanner.close();
            }
            log.debug(jsonResponse);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    private NotificationOneSignalResponse toResponse(
            String content, ResponseDataAPI data, UUID receiverId) {
        NotificationOneSignalResponse object = new NotificationOneSignalResponse();
        List<UUID> listReceiver = new ArrayList<>();
        listReceiver.add(receiverId);

        ContentResponse contentResponse = new ContentResponse();
        contentResponse.setEn(content);

        object.setContents(contentResponse);
        object.setData(data);
        object.setIncludeExternalUserIds(listReceiver);
        return object;
    }

    private String convertToJSONString(Object obj){
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(obj);
        } catch (Exception e) {
            log.error(e.getMessage());
            return null;
        }
    }
}
