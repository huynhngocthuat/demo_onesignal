package com.assetjapan.demo_onesignal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoOnesignalApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoOnesignalApplication.class, args);
    }

}
