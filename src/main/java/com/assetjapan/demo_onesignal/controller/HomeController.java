package com.assetjapan.demo_onesignal.controller;

import com.assetjapan.demo_onesignal.common.CommonConstant;
import com.assetjapan.demo_onesignal.service.PushNotificationOneSignalService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class HomeController {
    private final PushNotificationOneSignalService pushNotificationOneSignalService;

    @PostMapping("/external_id")
    public String sendNotification(@RequestParam String content, @RequestParam UUID receiverId) {
        try {
            pushNotificationOneSignalService.pushByExternalUserId(content, null, receiverId);
        } catch (Exception exception) {
            return CommonConstant.FAILURE;
        }
        return CommonConstant.SUCCESS;
    }

    @PostMapping("/filter")
    public String sendByFilter(@RequestParam String content, @RequestParam UUID receiverId) {
        try {
            pushNotificationOneSignalService.pushByFilter(null, receiverId, null);
        } catch (Exception exception) {
            return CommonConstant.FAILURE;
        }
        return CommonConstant.SUCCESS;
    }

    @PostMapping("/voip")
    public String sendByVoIP(@RequestParam String content, @RequestParam UUID receiverId) {
        try {
            pushNotificationOneSignalService.pushVoIP(content, null, receiverId);
        } catch (Exception exception) {
            return CommonConstant.FAILURE;
        }
        return CommonConstant.SUCCESS;
    }
}
