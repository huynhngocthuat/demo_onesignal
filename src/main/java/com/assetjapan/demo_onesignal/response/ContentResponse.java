package com.assetjapan.demo_onesignal.response;

import lombok.Data;

@Data
public class ContentResponse {
    private String en;
    private String vi;
    private String jp;
    private String pt;
}
