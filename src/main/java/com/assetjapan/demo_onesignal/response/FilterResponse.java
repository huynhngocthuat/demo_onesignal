package com.assetjapan.demo_onesignal.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class FilterResponse {
  private String field;
  private String key;
  private String relation;
  private Object value;

  public static FilterResponse filterLocation(Object value) {
    FilterResponse filterResponse = new FilterResponse();
    filterResponse.setField("tag");
    filterResponse.setKey("location");
    filterResponse.setRelation("=");
    filterResponse.setValue(value);
    return filterResponse;
  }
}
