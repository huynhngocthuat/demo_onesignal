package com.assetjapan.demo_onesignal.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

import java.util.List;
import java.util.UUID;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class NotificationOneSignalResponse {
  private String appId;
  private List<String> includedSegments;
  private Object data;
  private ContentResponse contents;
  private List<UUID> includeExternalUserIds;
  private List<FilterResponse> filters;
  private boolean androidBackgroundData;
  private boolean contentAvailable;
  private String apnsPushTypeOverride;
}
